//JS scope
var a = 'a_start';
let b = 'b_start';
const c = 'c_start';

function test(){
    var a = 'a';
    let b = 'b';
    // c  = 'c';
}
test();
console.log(a,b,c);


function test2(){
    if(1){
        var test_1 = '1';
        console.log(test_1);
    }
    console.log(test_1);
}
test2();