# Async & Performance

## Asynchrony: Now & Later

- A Program in Chunks
  - Async Console
- Event Loop
- Parallel Threading
  - Run-to-Completion
- Concurrency
  - Noninteracting
  - Interaction
  - Cooperation
- Jobs
- Statement Ordering

## Callbacks

- Continuations
- Sequential Brain
  - Doing Versus Planning
  - Nested/Chained Callbacks
- Trust Issues
  - Tale of Five Callbacks
  - Not Just Others’ Code
- Trying to Save Callbacks

## Promises

- What Is a Promise?
  - Future Value
  - Completion Event
- Thenable Duck Typing
- Promise Trust
  - Calling Too Early
  - Calling Too Late
  - Never Calling the Callback
  - Calling Too Few or Too Many Times
  - Failing to Pass Along Any Parameters/Environment
  - Swallowing Any Errors/Exceptions
  - Trustable Promise?
  - Trust Built
- Chain Flow
  - Terminology: Resolve, Fulfill, and Reject
- Error Handling
  - Pit of Despair
  - Uncaught Handling
  - Pit of Success
- Promise Patterns
  - Promise.all([ .. ])
  - Promise.race([ .. ])
  - Variations on all([ .. ]) and race([ .. ])
  - Concurrent Iterations
- Promise API Recap
  - new Promise(..) Constructor
  - Promise.resolve(..) and Promise.reject(..)
  - then(..) and catch(..)
  - Promise.all([ .. ]) and Promise.race([ .. ])
- Promise Limitations
  - Sequence Error Handling
  - Single Value
  - Single Resolution
  - Inertia
  - Promise Uncancelable
  - Promise Performance

## Generators

- Breaking Run-to-Completion
  - Input and Output
  - Multiple Iterators
- Generator-ing Values
  - Producers and Iterators
  - Iterables
  - Generator Iterator
- Iterating Generators Asynchronously
  - Synchronous Error Handling
- Generators + Promises
  - Promise-Aware Generator Runner
  - Promise Concurrency in Generators
- Generator Delegation
  - Why Delegation?
  - Delegating Messages
  - Delegating Asynchrony
  - Delegating Recursion
- Generator Concurrency
- Thunks
- s/promise/thunk/
- Pre-ES6 Generators
  - Manual Transformation
  - Automatic Transpilation

## Program Performance

- Web Workers
  - Worker Environment
  - Data Transfer
  - Shared Workers
  - Polyfilling Web Workers
- SIMD
- asm.js
  - How to Optimize with asm.js
  - asm.js Modules

## Benchmarking & Tuning

- Benchmarking
  - Repetition
  - Benchmark.js
- Context Is King
  - Engine Optimizations
- jsPerf.com
  - Sanity Check
- Writing Good Tests
- Microperformance
  - Not All Engines Are Alike
  - Big Picture
- Tail Call Optimization (TCO)

## Asynquence Library

- Sequences and Abstraction Design
- asynquence API
  - Steps
  - Errors
  - Parallel Steps
  - Forking Sequences
  - Combining Sequences
- Value and Error Sequences
- Promises and Callbacks
- Iterable Sequences
- Running Generators
  - Wrapped Generators

## Advanced Async Patterns

- Iterable Sequences
  - Extending Iterable Sequences
- Event Reactive
  - ES7 Observables
  - Reactive Sequences
- Generator Coroutine
  - State Machines
- Communicating Sequential Processes (CSP)
  - Message Passing
  - asynquence CSP emulation
