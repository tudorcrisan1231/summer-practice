# this & Object Prototypes

## This Or That?

- Why this?
- Confusions
  - Itself
  - Its Scope
- What’s this?

## This All Makes Sense Now!

- Call-Site
- Nothing but Rules - Default Binding
  - Implicit Binding
  - Explicit Binding
  - new Binding
- Everything in Order
  - Determining this
- Binding Exceptions
  - Ignored this
  - Indirection
  - Softening Binding
- Lexical this

## Objects

- Syntax
- Type
  - Built-in Objects
- Contents
  - Computed Property Names
  - Property Versus Method
  - Arrays
  - Duplicating Objects
  - Property Descriptors
  - Immutability
  - \[\[Get\]\]
  - \[\[Put\]\]
  - Getters and Setters
  - Existence
- Iteration

## Mixing (Up) “Class” Objects

- Class Theory
  - “Class” Design Pattern
  - JavaScript “Classes”
- Class Mechanics
  - Building
  - Constructor
- Class Inheritance
  - Polymorphism
  - Multiple Inheritance
- Mixins
  - Explicit Mixins
  - Implicit Mixins

## Prototypes

- \[\[Prototype\]\]
  - Object.prototype
  - Setting and Shadowing Properties
- “Class”
  - “Class” Functions
  - “Constructors”
  - Mechanics
- (Prototypal) Inheritance
  - Inspecting “Class” Relationships
- Object Links
  - Create()ing Links
  - Links as Fallbacks?

## Behavior Delegation

- Toward Delegation-Oriented Design
  - Class Theory
  - Delegation Theory
  - Mental Models Compared
- Classes Versus Objects
  - Widget “Classes”
  - Delegating Widget Objects
- Simpler Design
  - De-class-ified
- Nicer Syntax
  - Unlexical
- Introspection

## ES6 Class

- class
- class Gotchas
- Static > Dynamic?
