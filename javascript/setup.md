# Installing a Text Editor

## Visual Studio Code

- [Website](https://code.visualstudio.com/)

## VSCodium

- Builds Microsoft's vscode repository into freely-licensed binaries with a community-driven default configuration

  - [Website](https://vscodium.com/)
  - [Repository](https://github.com/VSCodium/vscodium)

- Installing the Package Manager - **brew** (https://brew.sh/)

  ```shell
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  ```

- Installing VSCodium

  ```shell
  brew install --cask vscodium
  ```
